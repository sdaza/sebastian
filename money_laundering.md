# Money Laundering

I use the Python package `networkx` to identify open triads that meet conditions to suspect the transaction is bridging.

To run the code use:

`python money_laundering.py 0.1 susp_transactions.txt susp_entities.txt`

where 0.1 corresponds to the *fee rate*.

I implement a multiprocessing solution, but it was too slow on my laptop. Still, I include that code.
