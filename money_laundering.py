import networkx as nx
import pandas as pd
import numpy as np
import sys
from itertools import product
import zipfile
from joblib import Parallel, delayed
import multiprocessing


# get data
sys.stdout.write('Reading the data...'+'\n')
zf = zipfile.ZipFile('data/transactions.zip')
df = pd.read_csv(zf.open('transactions.csv'),sep='|')
df.columns = df.columns.str.lower()

# check duplicates
# df.groupby(['transaction']).count().transaction.value_counts() # no duplicates

# get all dates
dates = df.timestamp.unique()

# get parameters from command line
fee = float(str(sys.argv[1]))
trans_file = sys.argv[2]
ent_file = sys.argv[3]

# get triads with no self-loops, transactions ids, and date
# function get triads using a focal node
# flexible to examine different network structures
def get_suspicious_triads(G, node, fee=0.10):
    '''
    Input:
    G - networkx object
    node -  focal node
    fee - fee rate
    Output: list with entities, transactions and date
    '''
    tr = [] # list to save results

    # get income and outgoing ties from focal node
    incoming = list(G.predecessors(node))
    outgoing = list(G.successors(node))

    if ((len(incoming)>0) & (len(outgoing)>0)):

        for n1,n2 in product(incoming, outgoing):

            if ((G.has_node(n1)) & (G.has_node(n2))
                    & (n1 != n2) & (n1 != node) & (n2 != node)):
                if ((G.has_edge(n1, node)) & (G.has_edge(node, n2))):

                    n_edges_1 = list(range(G.number_of_edges(n1,node)))
                    n_edges_2 = list(range(G.number_of_edges(node, n2)))

                    for e1, e2 in product(n_edges_1, n_edges_2): # for multiple transactions the same date
                        time1 = G.edges[n1,node,e1]['timestamp']
                        time2 = G.edges[node,n2,e2]['timestamp']
                        cond1 = time1 == time2 # double check time
                        prop_amount = G.edges[node,n2,e2]['amount'] / G.edges[n1,node,e1]['amount']
                        cond2 =  (prop_amount < 1.0) & (prop_amount >= (1.0-fee))

                        if ( cond1 & cond2 ):
                            trans1 = G.edges[n1,node,e1]['transaction']
                            trans2 = G.edges[node,n2,e2]['transaction']
                            tr.append([n1, node, n2, trans1, trans2, str(time1)])
    else:
        pass
    return list(filter(None,tr))

# function to combine records and create dataframe by date
# function using multiple processing
# def bridges_by_date_m(data, date, fee):
#     '''
#     Input:
#     data - pandas dataframe
#     dates - list of dates
#     fee - fee rate
#     Output:
#     pandas data frame
#     '''
#     sys.stdout.write(str(date)+'\n')
#     triads = []
#     G = nx.from_pandas_dataframe(data[data.timestamp==date],
#                                      'sender', 'receiver',
#                                      ['transaction', 'timestamp', 'amount'],
#                                      create_using=nx.MultiDiGraph())
#     for node in G.nodes:
#          triads.append(get_suspicious_triads(G, node, fee=fee))
#     return [y for x in list(filter(None,triads)) for y in x]


# using multiple cores
# too slow on my laptop
# num_cores = multiprocessing.cpu_count()
# sys.stdout.write('Looking for suspicious transactions...'+'\n')
# results = Parallel(n_jobs=num_cores)(delayed(bridges_by_date_m)(df, i, fee) for i in dates)

    # return pd.DataFrame(tr,
                        # columns=['sender', 'bridge', 'receiver', 'trans1', 'trans2', 'date'])

# function to combine records and create dataframe by date
def bridges_by_date(data, dates, fee):
    '''
    Input:
    data - pandas dataframe
    dates - list of dates
    fee - fee rate
    Output:
    pandas dataframe
    '''

    triads = []
    c = 0
    for date in dates:
        c=c+1
        if (c % 50) == 0: # print every 10 records
            sys.stdout.write(str(round(c/len(dates)*100,1))+ '%' + ' : ' + str(date)+'\n')

        # create networkx object
        G = nx.from_pandas_dataframe(data[data.timestamp==date],
                                     'sender', 'receiver',
                                     ['transaction', 'timestamp', 'amount'],
                                     create_using=nx.MultiDiGraph())

        for node in G.nodes:
            triads.append(get_suspicious_triads(G, node, fee=fee))

    ctriads = [y for x in list(filter(None,triads)) for y in x]
    return pd.DataFrame(ctriads,
                            columns=['sender', 'bridge', 'receiver', 'trans1', 'trans2', 'date'])



# run function
sys.stdout.write('Looking for suspicious transactions...'+'\n')
df_susp = bridges_by_date(df, dates, fee=fee)
# df_susp = pd.concat(results) # multiprocessing

sys.stdout.write('Saving result files...'+'\n')

# suspicious transactions
mtrans = pd.melt(df_susp, id_vars=['sender', 'bridge', 'receiver'], value_vars=['trans1', 'trans2'])
mtrans.drop_duplicates(['value'], inplace=True)
mtrans['value'].to_csv(trans_file, header=False, index=False)

# suspicious entities
ment = pd.melt(df_susp, id_vars=['trans1', 'trans2', 'date'], value_vars=['sender', 'bridge', 'receiver'])
mentg = ment.groupby('value').value.count()
mentg.sort_values(ascending=False).to_csv(ent_file, header=False, index=True)

sys.stdout.write('Done!'+'\n')
